export const GENDER = [
  {
    name: "MALE",
    value: "MALE",
  },
  {
    name: "FEMALE",
    value: "FEMALE",
  },
];

export const STATE = [
  {
    name: "JOHOR",
    value: "1",
  },
  {
    name: "KEDAH",
    value: "2",
  },
  {
    name: "KELANTAN",
    value: "3",
  },
  {
    name: "KUALA LUMPUR",
    value: "4",
  },
  {
    name: "MELAKA",
    value: "5",
  },
  {
    name: "NEGERI SEMBILAN",
    value: "6",
  },
  {
    name: "PAHANG",
    value: "7",
  },
  {
    name: "PERAK",
    value: "8",
  },
  {
    name: "PULAU PINANG",
    value: "9",
  },
  {
    name: "SABAH",
    value: "10",
  },
  {
    name: "SARAWAK",
    value: "11",
  },
  {
    name: "SELANGOR",
    value: "12",
  },
  {
    name: "TERENGGANU",
    value: "13",
  }
];

export const ROLE = [
  {
    name: "STAFF",
    value: "STA",
  },
  {
    name: "HEAD OF DEPARTMENT",
    value: "HOD",
  },
  {
    name: "ADMINISTRATOR",
    value: "ADM",
  }
];
  