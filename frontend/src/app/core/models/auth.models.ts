export class Employee {
    employeeId: number;
    username?: string;
    password: string;
    employeeName: string;
    employeeEmail: string;
    employeeRole: string;
    token?: string;
}
