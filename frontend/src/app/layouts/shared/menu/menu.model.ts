
export interface MenuItem {
    id?: number;
    label?: string;
    role?: string[];
    dept?: number[];
    icon?: string;
    link?: string;
    expanded?: boolean;
    subItems?: any;
    isTitle?: boolean;
    badge?: any;
    parentId?: number;
}
