import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    {
        label: 'Navigation',
        role: [],
        dept: [1,2,3,4,5,6,7,8,9,10], 
        isTitle: true
    },
    {
        label: 'Dashboard',
        role: [],
        dept: [], 
        icon: 'home',
        link: '/'
    },
    {
        label: 'Setup',
        role: [],
        dept: [1,2], 
        icon: 'settings',
        subItems: [
            {
                label: 'Employees',
                link: '/setup/employees'
            },
            {
                label: 'Branches',
                link: '/setup/branches'
            },
            {
                label: 'Departments',
                link: '/setup/departments'
            },
            {
                label: 'Positions',
                link: '/setup/positions'
            }
        ]
    },
    {
        label: 'Configurator',
        role: [],
        dept: [1,2], 
        icon: 'settings',
        subItems: [
            {
                label: 'Configurator Options',
                link: '/configurator/configurator-options'
            },
        ]
    },

];

