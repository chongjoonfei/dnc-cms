import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FlatpickrModule } from 'angularx-flatpickr';

import { UIModule } from '../shared/ui/ui.module';
import { PagesRoutingModule } from './pages-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';

import { EmployeesModule } from './admin/setup/employees/employees.module';
import { EmployeesRoutingModule } from './admin/setup/employees/employees-routing.module';

import { BranchesModule } from './admin/setup/branches/branches.module';
import { BranchesRoutingModule } from './admin/setup/branches/branches-routing.module';

import { DepartmentsModule } from './admin/setup/departments/departments.module';
import { DepartmentsRoutingModule } from './admin/setup/departments/departments-routing.module';

import { PositionsModule } from './admin/setup/positions/positions.module';
import { PositionsRoutingModule } from './admin/setup/positions/positions-routing.module';

import { ConfiguratorModule } from './configurator/configurator.module';
import { ConfiguratorRoutingModule } from './configurator/configurator-routing.module';

import { ClientsModule } from './admin/setup/clients/clients.module';
import { ClientsRoutingModule } from './admin/setup/clients/clients-routing.module';



@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    NgApexchartsModule,
    FlatpickrModule.forRoot(),
    UIModule,
    PagesRoutingModule,
    EmployeesModule,
    EmployeesRoutingModule,
    BranchesModule,
    BranchesRoutingModule,
    DepartmentsModule,
    DepartmentsRoutingModule,
    PositionsModule,
    PositionsRoutingModule,
    ClientsModule,
    ClientsRoutingModule,
    ConfiguratorModule,
    ConfiguratorRoutingModule,
  ]
})
export class PagesModule { }
