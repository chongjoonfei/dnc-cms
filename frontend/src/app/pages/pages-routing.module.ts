import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'setup/employees', loadChildren: () => import('./admin/setup/employees/employees.module').then(m => m.EmployeesModule) },
  { path: 'setup/branches', loadChildren: () => import('./admin/setup/branches/branches.module').then(m => m.BranchesModule) },
  { path: 'setup/departments', loadChildren: () => import('./admin/setup/departments/departments.module').then(m => m.DepartmentsModule) },
  { path: 'setup/positions', loadChildren: () => import('./admin/setup/positions/positions.module').then(m => m.PositionsModule) },
  { path: 'setup/clients', loadChildren: () => import('./admin/setup/clients/clients.module').then(m => m.ClientsModule) },

  { path: 'configurator', loadChildren: () => import('./configurator/configurator.module').then(m => m.ConfiguratorModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
