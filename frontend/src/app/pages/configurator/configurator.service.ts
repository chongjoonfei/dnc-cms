import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ConfiguratorService {

  constructor(
    private http: HttpClient
  ) { }

    listConfiguratorOptions(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/configurator/configurator-options`)
      .pipe(
          tap(_ => this.log('Fetched Configurator Options')),
          catchError(this.handleError('listConfiguratorOptions', []))
      );
    }

    getConfiguratorOptionById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/configurator/configurator-options/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched Configurator Option id: ${id}`))
      );
    }

    addConfiguratorOption(branch: any) {
        return this.http.post(`${environment.apiUrl}/configurator/configurator-options/create`, branch);
    }

    updateConfiguratorOption(id: any, branch: any) {
      return this.http.put(`${environment.apiUrl}/configurator/configurator-options/${id}`, branch)
      .pipe(
          tap(_ => this.log(`Updated Configurator Option id=${id}`))
      );
    }

    deleteConfiguratorOption(id: any) {
      return this.http.delete(`${environment.apiUrl}/configurator/configurator-options/${id}`);
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
