import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { PositionsService } from '../positions.service';



@Component({
  selector: 'app-positions-edit',
  templateUrl: './positions-edit.component.html',
  styleUrls: ['./positions-edit.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class PositionsEditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  positionsForm: FormGroup;

  submit: boolean;
  position: any;
  positionId: any = history.state.id;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private positionsService: PositionsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Positions', path: '/' },
      { label: 'Edit Position', path: '/', active: true }
    ];

    this.positionsForm = new FormGroup({
      positionName: new FormControl('', [Validators.required])
    });

    if (this.positionId === undefined) {
      this.router.navigate(['/setup/positions']);
    }

    this.getDepartment(this.positionId);
  }

  getDepartment(id: any) {
    this.positionsService.getPositionById(id).subscribe((data: any) => {
      this.position = data[0];
      this.positionsForm = this.formBuilder.group({
        departmentName: new FormControl( this.position.positionName, [Validators.required] )
      });
    }, error => console.log(error));
  }

  get f() { return this.positionsForm.controls; }

  onSubmit() {
    this.submit = true;

    if (this.positionsForm.invalid) {
      return;
    }

    this.positionsService.updatePosition(this.positionId, this.positionsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Position Saved', this.options);
        this.router.navigate(['/setup/positions']);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
