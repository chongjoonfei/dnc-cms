import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PositionsService {

  constructor(
    private http: HttpClient
  ) { }

    //Options
    listPositions(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/setup/positions`)
      .pipe(
          tap(_ => this.log('Fetched Positions')),
          catchError(this.handleError('listPositions', []))
      );
    }

    getPositionById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/setup/positions/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched Position id: ${id}`))
      );
    }

    addPosition(position: any) {
        return this.http.post(`${environment.apiUrl}/setup/positions/create`, position);
    }

    updatePosition(id: any, position: any) {
      return this.http.put(`${environment.apiUrl}/setup/positions/${id}`, position)
      .pipe(
          tap(_ => this.log(`Updated Position id=${id}`))
      );
    }

    deletePosition(id: any) {
      return this.http.delete(`${environment.apiUrl}/setup/positions/${id}`);
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
