import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { PositionsService } from '../positions.service';



@Component({
  selector: 'app-positions-add',
  templateUrl: './positions-add.component.html',
  styleUrls: ['./positions-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class PositionsAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  positionsForm: FormGroup;

  submit: boolean;
  position: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private positionsService: PositionsService
  ) {}


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Positions', path: '/' },
      { label: 'Add Position', path: '/', active: true }
    ];

    this.positionsForm = this.formBuilder.group({
      positionName: ['', [Validators.required]]
    });

    
  }

  get f() { return this.positionsForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.positionsForm.invalid) {
      return;
    }

    this.positionsService.addPosition(this.positionsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Position Added', this.options);
        this.router.navigate(['/setup/positions']);
      },
      error => {
        this.alertService.error(error);
      });

  }
}
