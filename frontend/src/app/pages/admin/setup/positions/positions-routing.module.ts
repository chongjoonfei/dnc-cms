import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PositionsComponent } from './positions.component';
import { PositionsAddComponent } from './positions-add/positions-add.component';
import { PositionsEditComponent } from './positions-edit/positions-edit.component';

const routes: Routes = [
    {
      path: '',
      component: PositionsComponent
    },
    {
      path: 'add',
      component: PositionsAddComponent
    },
    {
      path: 'edit',
      component: PositionsEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PositionsRoutingModule { }
