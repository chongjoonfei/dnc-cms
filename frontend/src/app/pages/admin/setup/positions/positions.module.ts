import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../../shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { SharedModule } from '../../../../shared/shared.module';
import { PositionsRoutingModule } from './positions-routing.module';

import { DataTablesModule } from 'angular-datatables';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDropdownModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { PositionsComponent } from './positions.component';
import { PositionsAddComponent } from './positions-add/positions-add.component';
import { PositionsEditComponent } from './positions-edit/positions-edit.component';


@NgModule({
    imports: [
      CommonModule,
      PositionsRoutingModule,
      CKEditorModule,
      SharedModule,
      UIModule,
      FormsModule,
      ReactiveFormsModule,
      NgbDropdownModule,
      NgbPaginationModule,
      NgbTypeaheadModule,
      AngularMultiSelectModule,
      NgMultiSelectDropDownModule.forRoot(),
      DataTablesModule
    ],
    declarations: [
      PositionsComponent,
      PositionsAddComponent,
      PositionsEditComponent
    ]
})

export class PositionsModule { }
