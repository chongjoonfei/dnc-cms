import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { take , first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { AlertService } from '../../../../shared/ui/alert';
import { ModalService } from '../../../../shared/ui/modal/modal.service';
import { PositionsService } from './positions.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss']
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class PositionsComponent implements OnInit {
  // bread crum data
  breadCrumbItems: Array<{}>;

  //DataTable
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  positions: any;
  message: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private modalService: ModalService,
    private alertService: AlertService,
    private positionsService: PositionsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Positions', path: '/' , active: true }
    ];
 

    //Initialize DataTable Options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.loadPositions();
  }

  doEdit(bid: any) {
    this.router.navigate(['/setup/positions/edit'], { state: { id: bid } });
  }

  openModal(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1)
    ).subscribe(result => {
        if (result) {
            this.positionsService.deletePosition(id)
            .pipe(first())
            .subscribe(() => this.loadPositions()
            );
            this.alertService.success('Position Deleted.', this.options);
        }
      });
  }

  /**
   * fetches the table value
   */
  private loadPositions() {
    return this.positionsService.listPositions()
    .subscribe((res: any) => {
        this.positions = res;
        if (this.isDtInitialized) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        } else {
            this.isDtInitialized = true;
            this.dtTrigger.next();
        }
    },
    error => {
      this.message = error;
    });
  }

}
