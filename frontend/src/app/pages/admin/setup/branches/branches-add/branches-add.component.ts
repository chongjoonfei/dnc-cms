import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { BranchesService } from '../branches.service';



@Component({
  selector: 'app-branches-add',
  templateUrl: './branches-add.component.html',
  styleUrls: ['./branches-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class BranchesAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  branchesForm: FormGroup;

  submit: boolean;
  branch: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private branchesService: BranchesService
  ) {}


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Branches', path: '/' },
      { label: 'Add Branch', path: '/', active: true }
    ];

    this.branchesForm = this.formBuilder.group({
      branchName: ['', [Validators.required]]
    });

    
  }

  get f() { return this.branchesForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.branchesForm.invalid) {
      return;
    }

    this.branchesService.addBranch(this.branchesForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Branch Added', this.options);
        this.router.navigate(['/setup/branches']);
      },
      error => {
        this.alertService.error(error);
      });

  }
}
