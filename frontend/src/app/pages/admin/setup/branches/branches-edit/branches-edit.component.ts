import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { BranchesService } from '../branches.service';



@Component({
  selector: 'app-branches-edit',
  templateUrl: './branches-edit.component.html',
  styleUrls: ['./branches-edit.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class BranchesEditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  branchesForm: FormGroup;

  submit: boolean;
  branch: any;
  branchId: any = history.state.id;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private branchesService: BranchesService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Branches', path: '/' },
      { label: 'Edit Branch', path: '/', active: true }
    ];

    this.branchesForm = new FormGroup({
      branchName: new FormControl('', [Validators.required])
    });

    if (this.branchId === undefined) {
      this.router.navigate(['/setup/branches']);
    }

    this.getBranch(this.branchId);
  }

  getBranch(id: any) {
    this.branchesService.getBranchById(id).subscribe((data: any) => {
      this.branch = data[0];
      this.branchesForm = this.formBuilder.group({
        branchName: new FormControl( this.branch.branchName, [Validators.required] )
      });
    }, error => console.log(error));
  }

  get f() { return this.branchesForm.controls; }

  onSubmit() {
    this.submit = true;

    if (this.branchesForm.invalid) {
      return;
    }

    this.branchesService.updateBranch(this.branchId, this.branchesForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Branch Saved', this.options);
        this.router.navigate(['/setup/branches']);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
