import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BranchesService {

  constructor(
    private http: HttpClient
  ) { }

    //Options
    listBranches(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/setup/branches`)
      .pipe(
          tap(_ => this.log('Fetched Branches')),
          catchError(this.handleError('listBranches', []))
      );
    }

    getBranchById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/setup/branches/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched Branch id: ${id}`))
      );
    }

    addBranch(branch: any) {
        return this.http.post(`${environment.apiUrl}/setup/branches/create`, branch);
    }

    updateBranch(id: any, branch: any) {
      return this.http.put(`${environment.apiUrl}/setup/branches/${id}`, branch)
      .pipe(
          tap(_ => this.log(`Updated Branch id=${id}`))
      );
    }

    deleteBranch(id: any) {
      return this.http.delete(`${environment.apiUrl}/setup/branches/${id}`);
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
