import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BranchesComponent } from './branches.component';
import { BranchesAddComponent } from './branches-add/branches-add.component';
import { BranchesEditComponent } from './branches-edit/branches-edit.component';

const routes: Routes = [
    {
      path: '',
      component: BranchesComponent
    },
    {
      path: 'add',
      component: BranchesAddComponent
    },
    {
      path: 'edit',
      component: BranchesEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BranchesRoutingModule { }
