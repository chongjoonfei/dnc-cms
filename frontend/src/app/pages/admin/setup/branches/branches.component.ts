import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { take , first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { AlertService } from '../../../../shared/ui/alert';
import { ModalService } from '../../../../shared/ui/modal/modal.service';
import { BranchesService } from './branches.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.scss']
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class BranchesComponent implements OnInit {
  // bread crum data
  breadCrumbItems: Array<{}>;

  //DataTable
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  branches: any;
  message: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private modalService: ModalService,
    private alertService: AlertService,
    private branchesService: BranchesService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Branches', path: '/' , active: true }
    ];
 

    //Initialize DataTable Options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.loadBranches();
  }

  doEdit(bid: any) {
    this.router.navigate(['/setup/branches/edit'], { state: { id: bid } });
  }

  openModal(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1)
    ).subscribe(result => {
        if (result) {
            this.branchesService.deleteBranch(id)
            .pipe(first())
            .subscribe(() => this.loadBranches()
            );
            this.alertService.success('Branch Deleted.', this.options);
        }
      });
  }

  /**
   * fetches the table value
   */
  private loadBranches() {
    return this.branchesService.listBranches()
    .subscribe((res: any) => {
        this.branches = res;
        if (this.isDtInitialized) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        } else {
            this.isDtInitialized = true;
            this.dtTrigger.next();
        }
    },
    error => {
      this.message = error;
    });
  }

}
