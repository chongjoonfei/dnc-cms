import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { EmployeesService } from '../employees.service';
import { BranchesService } from '../../branches/branches.service';
import { DepartmentsService } from '../../departments/departments.service';
import { PositionsService } from '../../positions/positions.service';

import { ROLE, GENDER, STATE } from '../../../../../core/constant/constant';


@Component({
  selector: 'app-employees-add',
  templateUrl: './employees-add.component.html',
  styleUrls: ['./employees-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class EmployeesAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  employeesForm: FormGroup;

  submit: boolean;
  employee: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  public roleList: Array<any>;
  public selectedRole: Array<any>;
  public roleSettings: any;

  public genderList: Array<any>;
  public selectedGender: Array<any>;
  public genderSettings: any;

  public branchList: Array<any>;
  public selectedBranch: Array<any>;
  public branchSettings: any;

  public departmentList: Array<any>;
  public selectedDepartment: Array<any>;
  public departmentSettings: any;

  public positionList: Array<any>;
  public selectedPosition: Array<any>;
  public positionSettings: any;

  public employeeList: Array<any>;
  public selectedEmployee: Array<any>;
  public employeeSettings: any;

  public stateList: Array<any>;
  public selectedState: Array<any>;
  public stateSettings: any;

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private employeesService: EmployeesService,
    private branchesService: BranchesService,
    private departmentsService: DepartmentsService,
    private positionsService: PositionsService,
    private datePipe: DatePipe
  ) {
    this.roleSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false
    };

    this.genderSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false
    };

    this.branchSettings = {
      singleSelection: true,
      idField: 'branchId',
      textField: 'branchName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.departmentSettings = {
      singleSelection: true,
      idField: 'departmentId',
      textField: 'departmentName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.positionSettings = {
      singleSelection: true,
      idField: 'positionId',
      textField: 'positionName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.employeeSettings = {
      singleSelection: true,
      idField: 'employeeId',
      textField: 'employeeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.stateSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Employees', path: '/' },
      { label: 'Add Employee', path: '/', active: true }
    ];

    this.employeesForm = this.formBuilder.group({
      employeeName: ['', [Validators.required]],
      employeeCode: ['', [Validators.required]],
      role: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      dob: ['', [Validators.required]],
      nric: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      branchId: ['', [Validators.required]],
      departmentId: ['', [Validators.required]],
      positionId: ['', [Validators.required]],
      employment: ['', [Validators.required]],
      hod: ['', [Validators.required]],
      phone: [''],
      mobile: ['', [Validators.required]],
      address1: ['', [Validators.required]],
      address2: [''],
      city: ['', [Validators.required]],
      postal: ['', [Validators.required]],
      stateId: ['', [Validators.required]],
      dateCreated: ['', [Validators.required]]
    });

    this.roleList = ROLE;
    this.genderList = GENDER;
    this.getBranches();
    this.getDepartments();
    this.getPositions();
    this.getEmployees();
    this.stateList = STATE;
  }

  getBranches() {
    this.branchesService.listBranches().subscribe((data: any) => {
      this.branchList = data;
    }, error => console.log(error));
  }

  getDepartments() {
    this.departmentsService.listDepartments().subscribe((data: any) => {
      this.departmentList = data;
    }, error => console.log(error));
  }

  getPositions() {
    this.positionsService.listPositions().subscribe((data: any) => {
      this.positionList = data;
    }, error => console.log(error));
  }

  getEmployees() {
    this.employeesService.listEmployees().subscribe((data: any) => {
      this.employeeList = data;
    }, error => console.log(error));
  }

  get f() { return this.employeesForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.employeesForm.invalid) {
      return;
    }


    this.employeesForm.value.dateCreated = this.datePipe.transform(this.employeesForm.value.dateCreated, 'yyyy-MM-dd');
    this.employeesForm.value.dob = this.datePipe.transform(this.employeesForm.value.dob, 'yyyy-MM-dd');

    this.employeesService.addEmployee(this.employeesForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Employee Added', this.options);
        this.router.navigate(['/setup/employees']);
      },
      error => {
        this.alertService.error(error);
      });
    
  }
}
