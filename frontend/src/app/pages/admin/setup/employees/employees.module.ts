import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { UIModule } from '../../../../shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../../../../shared/shared.module';
import { EmployeesRoutingModule } from './employees-routing.module';

import { DataTablesModule } from 'angular-datatables';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDropdownModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { EmployeesComponent } from './employees.component';
import { EmployeesAddComponent } from './employees-add/employees-add.component';
import { EmployeesEditComponent } from './employees-edit/employees-edit.component';



@NgModule({
    imports: [
      CommonModule,
      EmployeesRoutingModule,
      CKEditorModule,
      SharedModule,
      UIModule,
      FormsModule,
      ReactiveFormsModule,
      NgbDropdownModule,
      NgbPaginationModule,
      NgbTypeaheadModule,
      AngularMultiSelectModule,
      NgMultiSelectDropDownModule.forRoot(),
      FlatpickrModule.forRoot(),
      NgxMaskModule.forRoot(),
      DataTablesModule
    ],
    declarations: [
      EmployeesComponent,
      EmployeesAddComponent,
      EmployeesEditComponent
    ],
    providers: [DatePipe]
})

export class EmployeesModule { }
