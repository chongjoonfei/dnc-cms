import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(
    private http: HttpClient
  ) { }

    //Options
    listEmployees(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/setup/employees`)
      .pipe(
          tap(_ => this.log('Fetched employees')),
          catchError(this.handleError('listEmployees', []))
      );
    }

    getEmployeeById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/setup/employees/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched employee id: ${id}`))
      );
    }

    addEmployee(employee: any) {
        return this.http.post(`${environment.apiUrl}/setup/employees/create`, employee);
    }

    updateEmployee(id: any, employee: any) {
      return this.http.put(`${environment.apiUrl}/setup/employees/${id}`, employee)
      .pipe(
          tap(_ => this.log(`Updated employee id=${id}`))
      );
    }

    deleteEmployee(id: any) {
      return this.http.delete(`${environment.apiUrl}/setup/employees/${id}`);
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
