import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { take , first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { AlertService } from '../../../../shared/ui/alert';
import { ModalService } from '../../../../shared/ui/modal/modal.service';
import { EmployeesService } from './employees.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class EmployeesComponent implements OnInit {
  // bread crum data
  breadCrumbItems: Array<{}>;

  //DataTable
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  employees: any;
  message: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private modalService: ModalService,
    private alertService: AlertService,
    private employeesService: EmployeesService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Employees', path: '/' , active: true }
    ];
 

    //Initialize DataTable Options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.loadEmployees();
  }

  doEdit(eid: any) {
    this.router.navigate(['/setup/employees/edit'], { state: { id: eid } });
  }

  openModal(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1)
    ).subscribe(result => {
        if (result) {
            this.employeesService.deleteEmployee(id)
            .pipe(first())
            .subscribe(() => this.loadEmployees()
            );
            this.alertService.success('Employee Deleted.', this.options);
        }
      });
  }

  /**
   * fetches the table value
   */
  private loadEmployees() {
    return this.employeesService.listEmployees()
    .subscribe((res: any) => {
        this.employees = res;
        console.log(res);
        if (this.isDtInitialized) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        } else {
            this.isDtInitialized = true;
            this.dtTrigger.next();
        }
    },
    error => {
      this.message = error;
    });
  }

}
