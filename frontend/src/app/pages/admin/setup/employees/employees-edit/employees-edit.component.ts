import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { EmployeesService } from '../employees.service';
import { BranchesService } from '../../branches/branches.service';
import { DepartmentsService } from '../../departments/departments.service';
import { PositionsService } from '../../positions/positions.service';

import { ROLE, GENDER, STATE } from '../../../../../core/constant/constant';


@Component({
  selector: 'app-employees-edit',
  templateUrl: './employees-edit.component.html',
  styleUrls: ['./employees-edit.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class EmployeesEditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  employeesForm: FormGroup;

  submit: boolean;
  employee: any;
  employeeId: any = history.state.id;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  public roleList: Array<any>;
  public selectedRole: Array<any>;
  public roleSettings: any;

  public genderList: Array<any>;
  public selectedGender: Array<any>;
  public genderSettings: any;

  public branchList: Array<any>;
  public selectedBranch: Array<any>;
  public branchSettings: any;

  public departmentList: Array<any>;
  public selectedDepartment: Array<any>;
  public departmentSettings: any;

  public positionList: Array<any>;
  public selectedPosition: Array<any>;
  public positionSettings: any;

  public employeeList: Array<any>;
  public selectedEmployee: Array<any>;
  public employeeSettings: any;

  public stateList: Array<any>;
  public selectedState: Array<any>;
  public stateSettings: any;

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private employeesService: EmployeesService,
    private branchesService: BranchesService,
    private departmentsService: DepartmentsService,
    private positionsService: PositionsService,
    private datePipe: DatePipe
  ) {
    this.roleSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false
    };

    this.genderSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: false
    };

    this.branchSettings = {
      singleSelection: true,
      idField: 'branchId',
      textField: 'branchName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.departmentSettings = {
      singleSelection: true,
      idField: 'departmentId',
      textField: 'departmentName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.positionSettings = {
      singleSelection: true,
      idField: 'positionId',
      textField: 'positionName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.employeeSettings = {
      singleSelection: true,
      idField: 'employeeId',
      textField: 'employeeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };

    this.stateSettings = {
      singleSelection: true,
      idField: 'value',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true
    };
  }

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Employees', path: '/' },
      { label: 'Edit Employee', path: '/', active: true }
    ];

   

    if (this.employeeId === undefined) {
      this.router.navigate(['/setup/employees']);
    }

    this.getBranches();
    this.getDepartments();
    this.getPositions();
    this.getEmployees();
    this.roleList = ROLE;
    this.genderList = GENDER;
    this.stateList = STATE;
    this.getEmployee(this.employeeId);
  }

  getEmployee(id: any) {
    this.employeesService.getEmployeeById(id).subscribe((data: any) => {
      this.employee = data[0];

      this.selectedRole = ROLE.filter(function (role) {
          return role.value === data[0].role && role.value !== undefined;
      });


      this.selectedBranch = this.branchList.filter(function (branch) {
          return branch.branchId === data[0].branchId && branch.branchId !== undefined;
      });

      this.selectedDepartment = this.departmentList.filter(function (department) {
          return department.departmentId === data[0].departmentId && department.departmentId !== undefined;
      });

      this.selectedPosition = this.positionList.filter(function (position) {
          return position.positionId === data[0].positionId && position.positionId !== undefined;
      });

      /*
      const want = [1,2];
      this.selectedPosition = this.positionList.filter(function (e) {
          return want.includes(e.positionId);
      });
      */
      

      this.selectedEmployee = this.employeeList.filter(function (employee) {
          return employee.employeeId === data[0].hod && employee.employeeId !== undefined;
      });

      this.selectedGender = GENDER.filter(function (gender) {
          return gender.value === data[0].gender && gender.value !== undefined;
      });

      this.selectedState = STATE.filter(function (state) {
          return state.value === data[0].stateId.toString() && state.value !== undefined;
      });


      this.employeesForm = this.formBuilder.group({
        employeeName: new FormControl(this.employee.employeeName, [Validators.required]),
        employeeCode: new FormControl(this.employee.employeeCode, [Validators.required]),
        role: new FormControl(this.selectedRole, [Validators.required]),
        email: new FormControl(this.employee.email, [Validators.required, Validators.email]),
        dob: new FormControl(this.employee.dob, [Validators.required]),
        nric: new FormControl(this.employee.nric, [Validators.required]),
        gender: new FormControl(this.selectedGender, [Validators.required]),
        branchId: new FormControl(this.selectedBranch, [Validators.required]),
        departmentId: new FormControl(this.selectedDepartment, [Validators.required]),
        positionId: new FormControl(this.selectedPosition, [Validators.required]),
        employment: new FormControl(this.employee.employment, [Validators.required]),
        hod: new FormControl(this.selectedEmployee, [Validators.required]),
        phone: new FormControl(this.employee.phone),
        mobile: new FormControl(this.employee.mobile, [Validators.required]),
        address1: new FormControl(this.employee.address1, [Validators.required]),
        address2: new FormControl(this.employee.address2),
        city: new FormControl(this.employee.city, [Validators.required]),
        postal: new FormControl(this.employee.postal, [Validators.required]),
        stateId: new FormControl(this.selectedState, [Validators.required]),
        dateCreated: new FormControl(this.employee.dateCreated, [Validators.required]),
      });

    }, error => console.log(error));
  }

  getBranches() {
    this.branchesService.listBranches().subscribe((data: any) => {
      this.branchList = data;
    }, error => console.log(error));
  }

  getDepartments() {
    this.departmentsService.listDepartments().subscribe((data: any) => {
      this.departmentList = data;
    }, error => console.log(error));
  }

  getPositions() {
    this.positionsService.listPositions().subscribe((data: any) => {
      this.positionList = data;
    }, error => console.log(error));
  }

  getEmployees() {
    this.employeesService.listEmployees().subscribe((data: any) => {
      this.employeeList = data;
    }, error => console.log(error));
  }

  get f() { return this.employeesForm.controls; }

  onSubmit() {
    this.submit = true;

    if (this.employeesForm.invalid) {
      return;
    }

    console.log(this.employeesForm.value);
    this.employeesForm.value.dateCreated = this.datePipe.transform(this.employeesForm.value.dateCreated, 'yyyy-MM-dd');
    this.employeesForm.value.dob = this.datePipe.transform(this.employeesForm.value.dob, 'yyyy-MM-dd');

    this.employeesService.updateEmployee(this.employeeId, this.employeesForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Employee Saved', this.options);
        this.router.navigate(['/setup/employees']);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
