import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from './clients.component';
import { ClientsAddComponent } from './clients-add/clients-add.component';
import { ClientsEditComponent } from './clients-edit/clients-edit.component';

import { LorriesAddComponent } from './lorries/lorries-add/lorries-add.component';

const routes: Routes = [
    {
      path: '',
      component: ClientsComponent
    },
    {
      path: 'add',
      component: ClientsAddComponent
    },
    {
      path: 'edit',
      component: ClientsEditComponent
    },
    {
      path: 'lorries/:id/add',
      component: LorriesAddComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientsRoutingModule { }
