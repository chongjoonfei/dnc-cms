import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(
    private http: HttpClient
  ) { }

    //Options
    listClients(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/clients`)
      .pipe(
          tap(_ => this.log('Fetched employees')),
          catchError(this.handleError('listClients', []))
      );
    }

    getClientById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/clients/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched employee id: ${id}`))
      );
    }

    addClient(employee: any) {
        return this.http.post(`${environment.apiUrl}/clients/create`, employee);
    }

    updateClient(id: any, employee: any) {
      return this.http.put(`${environment.apiUrl}/clients/${id}`, employee)
      .pipe(
          tap(_ => this.log(`Updated employee id=${id}`))
      );
    }

    deleteClient(id: any) {
      return this.http.delete(`${environment.apiUrl}/clients/${id}`);
    }

    listLorriesById(id: any): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/clients/lorries/${id}`)
      .pipe(
          tap(_ => this.log('Fetched employees')),
          catchError(this.handleError('listLorriesById', []))
      );
    }

    addLorry(id: any, lorry: any) {
      return this.http.post(`${environment.apiUrl}/clients/lorries/create/${id}`, lorry);
  }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
