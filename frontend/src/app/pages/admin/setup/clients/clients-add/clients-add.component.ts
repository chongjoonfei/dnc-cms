import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { ClientsService } from '../clients.service';
import { AlertService } from '../../../../../shared/ui/alert';



@Component({
  selector: 'app-clients-add',
  templateUrl: './clients-add.component.html',
  styleUrls: ['./clients-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class ClientsAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  clientsForm: FormGroup;

  submit: boolean;
  client: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private clientsService: ClientsService
  ) {}


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Clients', path: '/' },
      { label: 'Add Client', path: '/', active: true }
    ];

    this.clientsForm = this.formBuilder.group({
      clientName: ['', [Validators.required]],
      clientEmail: ['', [Validators.required, Validators.email]],
      status: ['']
    });

  }

  get f() { return this.clientsForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.clientsForm.invalid) {
      return;
    }

    this.clientsService.addClient(this.clientsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Client Added', this.options);
        this.router.navigate(['/setup/clients']);
      },
      error => {
        this.alertService.error(error);
      });

  }
}
