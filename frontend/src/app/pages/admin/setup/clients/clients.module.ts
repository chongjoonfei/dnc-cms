import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../../shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { SharedModule } from '../../../../shared/shared.module';
import { ClientsRoutingModule } from './clients-routing.module';

import { DataTablesModule } from 'angular-datatables';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDropdownModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { ClientsComponent } from './clients.component';
import { ClientsAddComponent } from './clients-add/clients-add.component';
import { ClientsEditComponent } from './clients-edit/clients-edit.component';

import { LorriesAddComponent } from './lorries/lorries-add/lorries-add.component';



@NgModule({
    imports: [
      CommonModule,
      ClientsRoutingModule,
      CKEditorModule,
      SharedModule,
      UIModule,
      FormsModule,
      ReactiveFormsModule,
      NgbDropdownModule,
      NgbPaginationModule,
      NgbTypeaheadModule,
      AngularMultiSelectModule,
      NgMultiSelectDropDownModule.forRoot(),
      DataTablesModule
    ],
    declarations: [
      ClientsComponent,
      ClientsAddComponent,
      ClientsEditComponent,
      LorriesAddComponent
    ]
})

export class ClientsModule { }
