import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { take , first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { AlertService } from '../../../../shared/ui/alert';
import { ModalService } from '../../../../shared/ui/modal/modal.service';
import { ClientsService } from './clients.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class ClientsComponent implements OnInit {
  // bread crum data
  breadCrumbItems: Array<{}>;

  //DataTable
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  clients: any;
  message: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private modalService: ModalService,
    private alertService: AlertService,
    private clientsService: ClientsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Clients', path: '/' , active: true }
    ];
 

    //Initialize DataTable Options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.loadClients();
  }

  doEdit(cid: any) {
    this.router.navigate(['/setup/clients/edit'], { state: { id: cid } });
  }

  openModal(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1)
    ).subscribe(result => {
        if (result) {
            this.clientsService.deleteClient(id)
            .pipe(first())
            .subscribe(() => this.loadClients()
            );
            this.alertService.success('Clients Deleted.', this.options);
        }
      });
  }

  /**
   * fetches the table value
   */
  private loadClients() {
    return this.clientsService.listClients()
    .subscribe((res: any) => {
        this.clients = res;
        if (this.isDtInitialized) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        } else {
            this.isDtInitialized = true;
            this.dtTrigger.next();
        }
    },
    error => {
      this.message = error;
    });
  }

}
