import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { ClientsService } from '../clients.service';
import { AlertService } from '../../../../../shared/ui/alert';



@Component({
  selector: 'app-clients-edit',
  templateUrl: './clients-edit.component.html',
  styleUrls: ['./clients-edit.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class ClientsEditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  clientsForm: FormGroup;

  submit: boolean;
  client: any;
  lorries: any;
  clientId: any = history.state.id;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private clientsService: ClientsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Clients', path: '/' },
      { label: 'Edit Client', path: '/', active: true }
    ];

    this.clientsForm = new FormGroup({
      clientName: new FormControl('', [Validators.required]),
      clientEmail: new FormControl('', [Validators.required, Validators.email]),
      status: new FormControl(''),
    });

    if (this.clientId === undefined) {
      this.router.navigate(['/setup/clients']);
    }

    this.getClient(this.clientId);
    this.getLorries(this.clientId);
  }

  getClient(id: any) {
    this.clientsService.getClientById(id).subscribe((data: any) => {
      this.client = data;
      this.clientsForm = this.formBuilder.group({
        clientName: new FormControl( this.client.clientName, [Validators.required] ),
        clientEmail: new FormControl( this.client.clientEmail, [Validators.required] ),
        status: new FormControl( this.client.status )
      });
    }, error => console.log(error));
  }

  getLorries(id: any) {
    this.clientsService.listLorriesById(id).subscribe((data: any) => {
      this.lorries = data;
    }, error => console.log(error));
  }

  get f() { return this.clientsForm.controls; }

  onSubmit() {
    this.submit = true;

    if (this.clientsForm.invalid) {
      return;
    }

    this.clientsService.updateClient(this.clientId, this.clientsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Client Saved', this.options);
        this.router.navigate(['/setup/clients']);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
