import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { ClientsService } from '../../clients.service';
import { AlertService } from '../../../../../../shared/ui/alert';



@Component({
  selector: 'app-lorries-add',
  templateUrl: './lorries-add.component.html',
  styleUrls: ['./lorries-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class LorriesAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  lorriesForm: FormGroup;

  submit: boolean;
  id: any;
  lorry: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private clientsService: ClientsService
  ) {}


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Clients', path: '/' },
      { label: 'Edit Client', path: '/' },
      { label: 'Add Lorry', path: '/', active: true }
    ];

    this.id = this.route.snapshot.paramMap.get('id');

    this.lorriesForm = this.formBuilder.group({
      lorryPlate: ['', [Validators.required]]
    });

  }

  get f() { return this.lorriesForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.lorriesForm.invalid) {
      return;
    }

    this.clientsService.addLorry(this.id, this.lorriesForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Lorry Added', this.options);
        this.router.navigate(['/setup/clients/edit'], { state: { id: this.id } });
      },
      error => {
        this.alertService.error(error);
      });

  }
}
