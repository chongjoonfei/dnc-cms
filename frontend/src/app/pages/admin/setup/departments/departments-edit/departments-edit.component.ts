import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { DepartmentsService } from '../departments.service';



@Component({
  selector: 'app-departments-edit',
  templateUrl: './departments-edit.component.html',
  styleUrls: ['./departments-edit.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class DepartmentsEditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  departmentsForm: FormGroup;

  submit: boolean;
  department: any;
  departmentId: any = history.state.id;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private departmentsService: DepartmentsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Departments', path: '/' },
      { label: 'Edit Department', path: '/', active: true }
    ];

    this.departmentsForm = new FormGroup({
      departmentCode: new FormControl('', [Validators.required]),
      departmentName: new FormControl('', [Validators.required])
    });

    if (this.departmentId === undefined) {
      this.router.navigate(['/setup/departments']);
    }

    this.getDepartment(this.departmentId);
  }

  getDepartment(id: any) {
    this.departmentsService.getDepartmentById(id).subscribe((data: any) => {
      this.department = data[0];
      this.departmentsForm = this.formBuilder.group({
        departmentCode: new FormControl( this.department.departmentCode, [Validators.required] ),
        departmentName: new FormControl( this.department.departmentName, [Validators.required] )
      });
    }, error => console.log(error));
  }

  get f() { return this.departmentsForm.controls; }

  onSubmit() {
    this.submit = true;

    if (this.departmentsForm.invalid) {
      return;
    }

    this.departmentsService.updateDepartment(this.departmentId, this.departmentsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Department Saved', this.options);
        this.router.navigate(['/setup/departments']);
      },
      error => {
        this.alertService.error(error);
      });
  }
}
