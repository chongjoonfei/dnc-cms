import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertService } from '../../../../../shared/ui/alert';
import { DepartmentsService } from '../departments.service';



@Component({
  selector: 'app-departments-add',
  templateUrl: './departments-add.component.html',
  styleUrls: ['./departments-add.component.scss']
})

/**
 * Dashboard component - handling dashboard with sidear and content
 */
export class DepartmentsAddComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  departmentsForm: FormGroup;

  submit: boolean;
  department: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private departmentsService: DepartmentsService
  ) {}


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Departments', path: '/' },
      { label: 'Add Department', path: '/', active: true }
    ];

    this.departmentsForm = this.formBuilder.group({
      departmentCode: ['', [Validators.required]],
      departmentName: ['', [Validators.required]]
    });

    
  }

  get f() { return this.departmentsForm.controls; }

  onSubmit() {

    this.submit = true;
    this.alertService.clear();

    if (this.departmentsForm.invalid) {
      return;
    }

    this.departmentsService.addDepartment(this.departmentsForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Department Added', this.options);
        this.router.navigate(['/setup/departments']);
      },
      error => {
        this.alertService.error(error);
      });

  }
}
