import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../../shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { SharedModule } from '../../../../shared/shared.module';
import { DepartmentsRoutingModule } from './departments-routing.module';

import { DataTablesModule } from 'angular-datatables';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDropdownModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { DepartmentsComponent } from './departments.component';
import { DepartmentsAddComponent } from './departments-add/departments-add.component';
import { DepartmentsEditComponent } from './departments-edit/departments-edit.component';


@NgModule({
    imports: [
      CommonModule,
      DepartmentsRoutingModule,
      CKEditorModule,
      SharedModule,
      UIModule,
      FormsModule,
      ReactiveFormsModule,
      NgbDropdownModule,
      NgbPaginationModule,
      NgbTypeaheadModule,
      AngularMultiSelectModule,
      NgMultiSelectDropDownModule.forRoot(),
      DataTablesModule
    ],
    declarations: [
      DepartmentsComponent,
      DepartmentsAddComponent,
      DepartmentsEditComponent
    ]
})

export class DepartmentsModule { }
