import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

  constructor(
    private http: HttpClient
  ) { }

    //Options
    listDepartments(): Observable<any[]> {
      return this.http.get<any[]>(`${environment.apiUrl}/setup/departments`)
      .pipe(
          tap(_ => this.log('Fetched Departments')),
          catchError(this.handleError('listDepartments', []))
      );
    }

    getDepartmentById(id: any): Observable<any> {
      return this.http.get<any>(`${environment.apiUrl}/setup/departments/${id}`)
      .pipe(
          tap(_ => this.log(`Fetched Department id: ${id}`))
      );
    }

    addDepartment(department: any) {
        return this.http.post(`${environment.apiUrl}/setup/departments/create`, department);
    }

    updateDepartment(id: any, department: any) {
      return this.http.put(`${environment.apiUrl}/setup/departments/${id}`, department)
      .pipe(
          tap(_ => this.log(`Updated Department id=${id}`))
      );
    }

    deleteDepartment(id: any) {
      return this.http.delete(`${environment.apiUrl}/setup/departments/${id}`);
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);

        return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
