import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { take , first} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

import { AlertService } from '../../../../shared/ui/alert';
import { ModalService } from '../../../../shared/ui/modal/modal.service';
import { DepartmentsService } from './departments.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})

/**
 * Advanced table component - handling the advanced table with sidebar and content
 */
export class DepartmentsComponent implements OnInit {
  // bread crum data
  breadCrumbItems: Array<{}>;

  //DataTable
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  departments: any;
  message: any;

  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };

  constructor(
    private router: Router,
    private modalService: ModalService,
    private alertService: AlertService,
    private departmentsService: DepartmentsService
  ) {}

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Setup', path: '/' },
      { label: 'Departments', path: '/' , active: true }
    ];
 

    //Initialize DataTable Options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.loadDepartments();
  }

  doEdit(bid: any) {
    this.router.navigate(['/setup/departments/edit'], { state: { id: bid } });
  }

  openModal(id: any) {
    this.modalService.confirm(
      'Are you sure want to delete?'
    ).pipe(
      take(1)
    ).subscribe(result => {
        if (result) {
            this.departmentsService.deleteDepartment(id)
            .pipe(first())
            .subscribe(() => this.loadDepartments()
            );
            this.alertService.success('Department Deleted.', this.options);
        }
      });
  }

  /**
   * fetches the table value
   */
  private loadDepartments() {
    return this.departmentsService.listDepartments()
    .subscribe((res: any) => {
        this.departments = res;
        if (this.isDtInitialized) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
            });
        } else {
            this.isDtInitialized = true;
            this.dtTrigger.next();
        }
    },
    error => {
      this.message = error;
    });
  }

}
