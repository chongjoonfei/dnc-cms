const express = require('express');
const router = express.Router()
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('./_helpers/jwt');
const errorHandler = require('./_helpers/error-handler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes

//
//Admin
//
app.use('/setup/employees', require('./scripts/admin/setup/employees/employees.controller'));
app.use('/setup/branches', require('./scripts/admin/setup/branches/branches.controller'));
app.use('/setup/departments', require('./scripts/admin/setup/departments/departments.controller'));
app.use('/setup/positions', require('./scripts/admin/setup/positions/positions.controller'));

// global error handler
app.use(errorHandler);

module.exports = app;
