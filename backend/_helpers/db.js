const mysql = require('mysql');
const fs = require('fs');
const util = require('util');

const env = require('dotenv');

env.config();

const DBParam = {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT || "3306", 10),
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE,
    connectionLimit: 10
}


const pool = mysql.createPool(DBParam);

pool.getConnection(function(err, conn) {
    if (err) {
        console.log("Error connecting Database");
    }

    console.log("Database is connected");
});

pool.on("acquire", function(connection) {
    console.log("Connection %d acquired", connection.threadId);
});

pool.on("release", function(connection) {
    console.log("Connection %d released", connection.threadId);
});

pool.query = util.promisify(pool.query);

module.exports = pool;

