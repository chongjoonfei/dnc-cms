const expressJwt = require('express-jwt');
const env = require('dotenv');

env.config();
const secret = process.env.JWT_KEY;

module.exports = department;

function department(department = []) {

    if (typeof department === 'integer') {
        department = department;
    }

    return [

        // authenticate JWT token and attach user to request object (req.user)
        expressJwt({ secret }),

        // authorize based on user role
        (req, res, next) => {
            console.log(department);
            console.log(req.user.dept);
            console.log(department.includes(req.user.dept));
            if (department.length && !department.includes(req.user.dept)) {
                // user's role is not authorized
                return res.status(401).json({ message: 'Unauthorized' });
            }

            // authentication and authorization successful
            next();
        }
    ];
}