const expressJwt = require('express-jwt');
const mysql = require('mysql');
const DB = require('../_helpers/db');
const env = require('dotenv');

env.config();

module.exports = jwt;

function jwt() {
    const secret = process.env.JWT_KEY;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/setup/employees/authenticate'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const id = payload.sub;
    const table = 'employee';
    const field = 'employeeId';

    let selectQuery = "SELECT * FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, [table, field, id]);

    await DB.query(query, (err, employee) => {
        if(err) {
            console.log.error(err);
            return;
        }

        if(!employee) {
            return done(null, true);
        }

        done();
    });

}
