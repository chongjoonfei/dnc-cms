﻿const express = require('express');
const router = express.Router();
const authorize = require('../../../../_helpers/authorize');
const Role = require('../../../../_helpers/role');

const departmentsService = require('./departments.service');

// routes
router.get('/', list);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function list(req, res, next) {
  departmentsService.list()
        .then(departments => departments ? res.json(departments) : res.status(400))
        .catch(err => next(err));
}

function getById(req, res, next) {
  departmentsService.getById(req.params.id)
      .then(department => department ? res.json(department) : res.sendStatus(404))
      .catch(err => next(err));
}

function create(req, res, next) {
  departmentsService.create(req.body)
  .then(() => res.json({}))
  .catch(err => next(err));
}

function update(req, res, next) {
  departmentsService.update(req.params.id, req.body)
      .then(() => res.json({}))
      .catch(err => next(err));
}

function _delete(req, res, next) {
  departmentsService.delete(req.params.id)
      .then(() => res.json({}))
      .catch(err => next(err))
}
