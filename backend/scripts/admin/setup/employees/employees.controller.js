﻿const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mysql = require('mysql');
const DB = require('../../../../_helpers/db');
const authorize = require('../../../../_helpers/authorize');
const department = require('../../../../_helpers/department');

const employeesService = require('../employees/employees.service');
const Role = require('../../../../_helpers/role');

// routes
router.post('/authenticate', authenticate);
router.get('/', department([1,2]), list);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function authenticate(req, res, next) {
  employeesService.authenticate(req.body)
        .then(employee => employee ? res.json(employee) : res.status(400).json({ message: 'Authentication Failed!' }))
        .catch(err => next(err));
}

function list(req, res, next) {
  employeesService.list()
        .then(employees => employees ? res.json(employees) : res.status(400))
        .catch(err => next(err));
}

function getById(req, res, next) {
  employeesService.getById(req.params.id)
      .then(employee => employee ? res.json(employee) : res.sendStatus(404))
      .catch(err => next(err));
}

function create(req, res, next) {
  employeesService.create(req.body)
  .then(() => res.json({}))
  .catch(err => next(err));
}

function update(req, res, next) {
  employeesService.update(req.params.id, req.body)
      .then(() => res.json({}))
      .catch(err => next(err));
}

function _delete(req, res, next) {
  employeesService.delete(req.params.id)
      .then(() => res.json({}))
      .catch(err => next(err))
}
