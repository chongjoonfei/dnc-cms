﻿const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mysql = require('mysql');
const DB = require('../../../../_helpers/db');
const env = require('dotenv');

env.config();

module.exports = {
    authenticate,
    list,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ email, password }) {
    //console.log(bcrypt.hashSync('yyng20@)', 10));

    let selectQuery = "SELECT * FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['employee', 'email', email]);

    const employee = await DB.query(query);

    if (employee && bcrypt.compareSync(password, employee[0].password)) {
        const { hash, ...userWithoutHash } = employee;
        const token = jwt.sign({ sub: employee[0].employeeId, role: employee[0].role, dept: employee[0].departmentId }, process.env.JWT_KEY);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function list() {
    let selectQuery = "SELECT * FROM ??";
    let query = mysql.format(selectQuery, ['employee']);

    return await DB.query(query);
}

async function getById(id) {
    let selectQuery = "SELECT * FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['employee', 'employeeId', id]);

    return await DB.query(query);
  }
  
async function create(employeeParam) {
    console.log(employeeParam.departmentId[0].departmentId);
    var post  = {
        employeeName: employeeParam.employeeName.toUpperCase(),
        employeeCode: employeeParam.employeeCode,
        role: employeeParam.role[0].value,
        email: employeeParam.email,
        password: bcrypt.hashSync(employeeParam.password, 10),
        dob: employeeParam.dob,
        nric: employeeParam.nric,
        gender: employeeParam.gender[0].value,
        branchId: employeeParam.branchId[0].branchId,
        departmentId: employeeParam.departmentId[0].departmentId,
        positionId: employeeParam.positionId[0].positionId,
        employment: employeeParam.employment,
        hod: employeeParam.hod[0].employeeId,
        phone: employeeParam.phone,
        mobile: employeeParam.mobile,
        address1: employeeParam.address1.toUpperCase(),
        address2: employeeParam.address2.toUpperCase(),
        city: employeeParam.city.toUpperCase(),
        postal: employeeParam.postal,
        stateId: employeeParam.stateId[0].value,
        dateCreated: employeeParam.dateCreated
    };
    let selectQuery = "INSERT INTO ?? SET ?";
    let query = mysql.format(selectQuery, ['employee', post]);

    return await DB.query(query);
}

async function update(id, employeeParam) {
    var post  = {
        employeeName: employeeParam.employeeName.toUpperCase(),
        employeeCode: employeeParam.employeeCode,
        role: employeeParam.role[0].value,
        email: employeeParam.email,
        dob: employeeParam.dob,
        nric: employeeParam.nric,
        gender: employeeParam.gender[0].value,
        branchId: employeeParam.branchId[0].branchId,
        departmentId: employeeParam.departmentId[0].departmentId,
        positionId: employeeParam.positionId[0].positionId,
        employment: employeeParam.employment,
        hod: employeeParam.hod[0].employeeId,
        phone: employeeParam.phone,
        mobile: employeeParam.mobile,
        address1: employeeParam.address1.toUpperCase(),
        address2: employeeParam.address2.toUpperCase(),
        city: employeeParam.city.toUpperCase(),
        postal: employeeParam.postal,
        stateId: employeeParam.stateId[0].value,
        dateCreated: employeeParam.dateCreated
    };

    let selectQuery = "UPDATE ?? SET ? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['employee', post, 'employeeId', id]);

    return await DB.query(query);
}

async function _delete(id) {
    let selectQuery = "DELETE FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['employee', 'employeeId', id]);

    return await DB.query(query);
}
  