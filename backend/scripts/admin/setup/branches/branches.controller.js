﻿const express = require('express');
const router = express.Router();
const authorize = require('../../../../_helpers/authorize');
const Role = require('../../../../_helpers/role');

const branchesService = require('./branches.service');

// routes
router.get('/', list);
router.get('/:id', getById);
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function list(req, res, next) {
  branchesService.list()
        .then(branches => branches ? res.json(branches) : res.status(400))
        .catch(err => next(err));
}

function getById(req, res, next) {
  branchesService.getById(req.params.id)
      .then(branch => branch ? res.json(branch) : res.sendStatus(404))
      .catch(err => next(err));
}

function create(req, res, next) {
  branchesService.create(req.body)
  .then(() => res.json({}))
  .catch(err => next(err));
}

function update(req, res, next) {
  branchesService.update(req.params.id, req.body)
      .then(() => res.json({}))
      .catch(err => next(err));
}

function _delete(req, res, next) {
  branchesService.delete(req.params.id)
      .then(() => res.json({}))
      .catch(err => next(err))
}
