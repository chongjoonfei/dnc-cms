﻿const mysql = require('mysql');
const DB = require('../../../../_helpers/db');
const env = require('dotenv');

env.config();

module.exports = {
    list,
    getById,
    create,
    update,
    delete: _delete
};

async function list() {
    let selectQuery = "SELECT * FROM ??";
    let query = mysql.format(selectQuery, ['branch']);

    return await DB.query(query);
}

async function getById(id) {
    let selectQuery = "SELECT * FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['branch', 'branchId', id]);

    return await DB.query(query);
  }
  
async function create(branchParam) {
    var post  = {
        branchName: branchParam.branchName.toUpperCase(),
    };
    let selectQuery = "INSERT INTO ?? SET ?";
    let query = mysql.format(selectQuery, ['branch', post]);

    return await DB.query(query);
}
  
async function update(id, branchParam) {
    var post  = {
        branchName: branchParam.branchName.toUpperCase(),
    };
    let selectQuery = "UPDATE ?? SET ? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['branch', post, 'branchId', id]);

    return await DB.query(query);
}
  
async function _delete(id) {
    let selectQuery = "DELETE FROM ?? WHERE ?? = ?";
    let query = mysql.format(selectQuery, ['branch', 'branchId', id]);

    return await DB.query(query);
}
  